const ActionType = {
  LOGIN: 'login',
  REGISTER: 'register',
  LOAD_PRODUCTS: 'load-products',
  LOAD_PRODUCT: 'load-product',
  ADD_PRODUCT: 'add-product',
  DELETE_PRODUCT: 'delete-product',
  LOGOUT: 'logout',
};

export {ActionType};
