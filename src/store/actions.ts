import {AppDispatch, RootState} from './store';
import {appService} from '../services/service';
import {createAction, createAsyncThunk} from '@reduxjs/toolkit';
import {User} from '../data/models/User';
import {Login} from '../data/models/Login';
import {Register} from '../data/models/Register';
import {ActionType} from './common';
import {Errors} from '../common/enums/http/errors';
import {Filters} from '../data/models/Filters';
import {ProductInList} from '../data/models/ProductInList';
import {Product} from '../data/models/Product';
import {Asset} from 'react-native-image-picker';
import {Picture} from '../data/models/Picture';

interface ThunkApiModel {
  dispatch: AppDispatch;
  state: RootState;
  rejectValue: string;
  extra: {
    service: typeof appService;
  };
}

interface LoadProducts {
  token: string;
  filters: Filters;
  isLoadMore?: boolean;
}

interface LoadProduct {
  token: string;
  productId: number;
}

interface AddProduct {
  token: string;
  title: string;
  price: string;
  description: string;
  images: Asset[];
}

interface DeleteProduct {
  token: string;
  id: number;
  images: Picture[] | undefined;
}

const login = createAsyncThunk<User, Login, ThunkApiModel>(
  ActionType.LOGIN,
  async (userData, {rejectWithValue, extra: {service}}) => {
    try {
      const {token} = await service.login(userData);
      const currentUser = await service.getCurrentUser(token);

      return {...currentUser, token};
    } catch (error: any) {
      return rejectWithValue(Errors.LOGIN_FAILED);
    }
  },
);

const logout = createAction(ActionType.LOGOUT);

const register = createAsyncThunk<User, Register, ThunkApiModel>(
  ActionType.REGISTER,
  async (userData, {extra: {service}}) => {
    await service.register(userData);
    const {token} = await service.login({
      email: userData.email,
      password: userData.password,
    });
    if (userData.avatar) {
      await service.addAvatar(token, userData.avatar);
    }

    const currentUser = await service.getCurrentUser(token);

    return {...currentUser, token};
  },
);

const loadProducts = createAsyncThunk<
  {products: ProductInList[]; isLoadMore: boolean | undefined},
  LoadProducts,
  ThunkApiModel
>(
  ActionType.LOAD_PRODUCTS,
  async ({token, filters, isLoadMore}, {extra: {service}}) => {
    const products = await service.loadProducts(token, filters);

    return {products, isLoadMore};
  },
);

const loadProduct = createAsyncThunk<Product, LoadProduct, ThunkApiModel>(
  ActionType.LOAD_PRODUCT,
  async ({token, productId}, {extra: {service}}) => {
    const product = await service.loadProduct(token, productId);

    return product;
  },
);

const addProduct = createAsyncThunk<Product, AddProduct, ThunkApiModel>(
  ActionType.ADD_PRODUCT,
  async ({token, title, price, description, images}, {extra: {service}}) => {
    const addedProduct = await service.addProduct(token, {
      title,
      price,
      description,
    });
    if (images && images.length > 0) {
      for (let i = 0; i < images.length; i++) {
        await service.addProductPicture(token, images[i], addedProduct.id);
      }
    }

    const product: any = await service.loadProduct(token, addedProduct.id);
    return product;
  },
);

const deleteProduct = createAsyncThunk<number, DeleteProduct, ThunkApiModel>(
  ActionType.DELETE_PRODUCT,
  async ({token, id, images}, {extra: {service}}) => {
    if (images) {
      for (let i = 0; i < images.length; i++) {
        await service.deleteProductPicture(token, id, images[i].id);
      }
    }

    await service.deleteProduct(token, id);

    return id;
  },
);

export {
  login,
  register,
  loadProducts,
  loadProduct,
  addProduct,
  deleteProduct,
  logout,
};
