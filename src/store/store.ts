import {configureStore} from '@reduxjs/toolkit';
import {appService} from '../services/service';
import {AppReducer} from './reducer';

const store = configureStore({
  reducer: AppReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      thunk: {
        extraArgument: {
          service: appService,
        },
      },
    }),
});

export type RootState = ReturnType<typeof AppReducer>;
export type AppDispatch = typeof store.dispatch;
export type AppStore = typeof store;

export default store;
