import {createReducer, isAnyOf} from '@reduxjs/toolkit';
import {Product} from '../data/models/Product';
import {ProductInList} from '../data/models/ProductInList';
import {User} from '../data/models/User';
import * as Actions from './actions';

interface AppState {
  currentUser: User | undefined;
  products: ProductInList[];
  openedProduct: Product | undefined;
  preloaderAuth: boolean;
  preloader: boolean;
  errorMessage: string | undefined;
}

const initialState: AppState = {
  currentUser: undefined,
  products: [],
  openedProduct: undefined,
  preloaderAuth: false,
  preloader: false,
  errorMessage: undefined,
};

const AppReducer = createReducer(initialState, builder => {
  builder.addCase(Actions.logout, state => {
    state.currentUser = undefined;
    state.products = [];
  });
  builder.addCase(Actions.deleteProduct.fulfilled, (state, action) => {
    const productId = action.payload;

    const deleteProductIndex = state.products.findIndex(
      product => product.id === productId,
    );
    state.products.splice(deleteProductIndex, 1);
    state.openedProduct = undefined;
  });
  builder.addCase(Actions.addProduct.fulfilled, (state, action) => {
    const product = action.payload;

    const newProduct = {
      id: product.id,
      title: product.title,
      price: product.price,
      description: product.description,
      picture:
        product.pictures && product.pictures.length > 0
          ? product.pictures[0].url
          : undefined,
    };
    state.products.push(newProduct);
    state.preloader = false;
  });
  builder.addCase(Actions.loadProduct.fulfilled, (state, action) => {
    const product = action.payload;

    state.preloader = false;
    state.openedProduct = product;
  });
  builder.addCase(Actions.loadProducts.fulfilled, (state, action) => {
    const {products, isLoadMore} = action.payload;

    state.preloader = false;
    if (isLoadMore) {
      state.products.push(...products);
    } else {
      state.products = products;
    }
  });
  builder.addCase(Actions.login.rejected, (state, action) => {
    const errorMessage = action.payload;

    state.preloaderAuth = false;
    state.errorMessage = errorMessage;
  });
  builder.addMatcher(
    isAnyOf(Actions.login.fulfilled, Actions.register.fulfilled),
    (state, action) => {
      const currentUser = action.payload;
      state.preloaderAuth = false;
      state.currentUser = currentUser;
    },
  );
  builder.addMatcher(
    isAnyOf(Actions.login.pending, Actions.register.pending),
    state => {
      state.preloaderAuth = true;
      state.errorMessage = undefined;
    },
  );
  builder.addMatcher(
    isAnyOf(Actions.loadProduct.pending, Actions.addProduct.pending),
    state => {
      state.preloader = true;
    },
  );
});

export {AppReducer};
