import {Picture} from './Picture';
import {Seller} from './Seller';

export interface Product {
  id: number;
  title: string;
  price: number;
  description: string;
  createdAt: string;
  pictures?: Picture[];
  seller: Seller;
}
