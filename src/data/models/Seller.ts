export interface Seller {
  name: string;
  email: string;
  phoneNumber: string;
  avatar?: string;
}
