export interface ProductInList {
  id: number;
  title: string;
  price: number;
  description: string;
  picture?: string;
}
