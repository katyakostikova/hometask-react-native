export interface User {
  id: number;
  token: string;
  email: string;
  name: string;
  phoneNumber: string;
}
