import {API_PATH} from '../common/enums/http/api-path';
import {ContentType} from '../common/enums/http/content-type';
import {HttpMethod} from '../common/enums/http/http-method';
import {Filters} from '../data/models/Filters';
import {Login} from '../data/models/Login';
import {Register} from '../data/models/Register';
import {AddProduct} from '../data/models/AddProduct';
import {Asset} from 'react-native-image-picker';

class AppService {
  private apiPath: string;

  constructor(apiPath: string) {
    this.apiPath = apiPath;
  }

  login(userLoginData: Login) {
    return fetch(`${this.apiPath}/Auth/login`, {
      method: HttpMethod.POST,
      headers: {
        'Content-Type': ContentType.JSON,
      },
      body: JSON.stringify(userLoginData),
    }).then(res => res.json());
  }

  getCurrentUser(token: string) {
    return fetch(`${this.apiPath}/Users/details`, {
      method: HttpMethod.GET,
      headers: {
        authorization: `Bearer ${token}`,
      },
    }).then(res => res.json());
  }

  register(userRegisterData: Register) {
    return fetch(`${this.apiPath}/Auth/register`, {
      method: HttpMethod.POST,
      headers: {
        'Content-Type': ContentType.JSON,
      },
      body: JSON.stringify(userRegisterData),
    }).then(res => res.json());
  }

  addAvatar(token: string, file: Asset) {
    return fetch(`${this.apiPath}/Users/avatar/asString`, {
      method: HttpMethod.POST,
      headers: {
        'Content-Type': ContentType.JSON,
        authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        fileName: file.fileName,
        content: file.base64,
      }),
    }).then(res => res.json());
  }

  loadProducts(token: string, {page, perPage, filter}: Filters) {
    return fetch(
      `${this.apiPath}/Products?page=${page}&perPage=${perPage}&filter=${filter}`,
      {
        method: HttpMethod.GET,
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
    ).then(res => res.json());
  }

  loadProduct(token: string, id: number) {
    return fetch(`${this.apiPath}/Products/${id}`, {
      method: HttpMethod.GET,
      headers: {
        authorization: `Bearer ${token}`,
      },
    }).then(res => res.json());
  }

  addProduct(token: string, productData: AddProduct) {
    return fetch(`${this.apiPath}/Products/`, {
      method: HttpMethod.POST,
      headers: {
        authorization: `Bearer ${token}`,
        'Content-Type': ContentType.JSON,
      },
      body: JSON.stringify(productData),
    }).then(res => res.json());
  }

  addProductPicture(token: string, file: Asset, id: number) {
    return fetch(`${this.apiPath}/Products/${id}/Pictures/asString`, {
      method: HttpMethod.POST,
      headers: {
        'Content-Type': ContentType.JSON,
        authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        fileName: file.fileName,
        content: file.base64,
      }),
    }).then(res => res.json());
  }

  deleteProductPicture(token: string, productId: number, pictureId: number) {
    return fetch(
      `${this.apiPath}/Products/${productId}/Pictures/${pictureId}`,
      {
        method: HttpMethod.DELETE,
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
    );
  }

  deleteProduct(token: string, id: number) {
    return fetch(`${this.apiPath}/Products/${id}`, {
      method: HttpMethod.DELETE,
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
  }
}

const appService = new AppService(API_PATH);

export {appService};
