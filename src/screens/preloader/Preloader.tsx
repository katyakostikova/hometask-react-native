import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import {styles} from './styles';

const Preloader = () => {
  const {colors} = useTheme();
  return (
    <View style={styles.screen}>
      <ActivityIndicator color={colors.primary} size="large" />
    </View>
  );
};

export default Preloader;
