import {RouteProp, useNavigation, useRoute} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {Button, ScrollView, View} from 'react-native';
import {ProductDetailsScreenRouteProp} from '../../common/types/navigation';
import Header from '../../components/product-details/header/Header';
import PicturesGallery from '../../components/product-details/pictures-gallery/PicturesGallery';
import ProductInfo from '../../components/product-details/product-info/ProductInfo';
import SellerInfo from '../../components/product-details/seller/SellerInfo';
import {AppColors} from '../../config/app-colors';
import {useAppDispatch, useAppSelector} from '../../hooks/redux';
import * as appActionCreator from '../../store/actions';
import Preloader from '../preloader/Preloader';
import {styles} from './styles';

const ProductDetailsScreen = () => {
  const {productId} =
    useRoute<RouteProp<ProductDetailsScreenRouteProp>>().params;
  const {currentUser, openedProduct, preloader} = useAppSelector(state => ({
    currentUser: state.currentUser,
    openedProduct: state.openedProduct,
    preloader: state.preloader,
  }));
  const dispatch = useAppDispatch();
  const navigation = useNavigation();

  const handleDeleteProduct = () => {
    if (currentUser && openedProduct) {
      dispatch(
        appActionCreator.deleteProduct({
          token: currentUser.token,
          id: productId,
          images: openedProduct.pictures,
        }),
      );
      navigation.goBack();
    }
  };

  useEffect(() => {
    if (currentUser) {
      dispatch(
        appActionCreator.loadProduct({token: currentUser.token, productId}),
      );
    }
  }, [currentUser, dispatch, productId]);

  if (preloader) {
    return <Preloader />;
  }

  return (
    <View style={styles.screen}>
      <Header />
      <ScrollView
        contentContainerStyle={styles.productContent}
        showsVerticalScrollIndicator={false}>
        <PicturesGallery pictures={openedProduct?.pictures} />
        {currentUser?.phoneNumber === openedProduct?.seller.phoneNumber && (
          <View style={styles.deleteButton}>
            <Button
              title="Delete product"
              onPress={handleDeleteProduct}
              color={AppColors.ERROR_TEXT}
            />
          </View>
        )}
        <ProductInfo
          title={openedProduct?.title}
          price={openedProduct?.price}
          description={openedProduct?.description}
        />
      </ScrollView>
      <SellerInfo seller={openedProduct?.seller} />
    </View>
  );
};

export default ProductDetailsScreen;
