import {useNavigation, useTheme} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Button,
  Image,
  ScrollView,
  Alert,
  Keyboard,
} from 'react-native';
import {Asset} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {IconNames} from '../../common/enums/components/icon-names';
import {IconSizes} from '../../common/enums/components/icon-sizes';
import TextInputApp from '../../components/text-input/TextInputAuth';
import {loadImages} from '../../helpers/images/image-from-library';
import {useAppDispatch, useAppSelector} from '../../hooks/redux';
import * as appActionCreator from '../../store/actions';
import {styles} from './styles';

const AddProductScreen = () => {
  const [title, setTitle] = useState('');
  const [price, setPrice] = useState('');
  const [description, setDescription] = useState('');

  const [images, setImages] = useState<Asset[]>([]);
  const [isUploading, setIsUploading] = useState(false);

  const navigation = useNavigation();
  const dispatch = useAppDispatch();
  const currentUser = useAppSelector(state => state.currentUser);
  const {colors} = useTheme();

  const handleCancel = () => {
    navigation.goBack();
  };

  const handleAddProduct = () => {
    if (
      title.trim().length < 1 ||
      price.trim().length < 1 ||
      description.trim().length < 1
    ) {
      Alert.alert('Invalid data', 'Please enter valid data');
      return;
    }
    if (currentUser) {
      dispatch(
        appActionCreator.addProduct({
          token: currentUser.token,
          title,
          price,
          description,
          images,
        }),
      );
      navigation.goBack();
    }
  };

  const handleImageUpload = async () => {
    setIsUploading(true);
    const file = await loadImages();
    if (!file) {
      setIsUploading(false);
      return;
    }
    setImages(state => [...state, file]);
    setIsUploading(false);
  };

  const removeImage = (index: number) => {
    const imagesArr = [...images];
    imagesArr.splice(index, 1);
    setImages(imagesArr);
  };

  const dismissKeyboard = () => {
    Keyboard.dismiss();
  };

  const renderImage = (image: Asset, index: number) => {
    return (
      <TouchableWithoutFeedback onPress={() => removeImage(index)} key={index}>
        <View>
          <Image source={{width: 55, height: 55, uri: image.uri}} />
          <View style={styles.removeIcon}>
            <Icon
              name={IconNames.DELETE}
              size={IconSizes.DELETE_IMAGE_ICON}
              color={'#ffffff'}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  return (
    <TouchableWithoutFeedback onPress={dismissKeyboard}>
      <View style={styles.screen}>
        <View style={{...styles.formContainer, backgroundColor: colors.card}}>
          <TextInputApp
            value={title}
            placeholder="Enter title"
            onChange={setTitle}
          />
          <TextInputApp
            value={price}
            placeholder="Enter price"
            keyboardType="number-pad"
            onChange={setPrice}
          />
          <TextInputApp
            value={description}
            placeholder="Enter description"
            onChange={setDescription}
          />
          <Button
            title="Add pictures"
            onPress={handleImageUpload}
            disabled={isUploading}
            color={colors.primary}
          />
          <View style={styles.imageScrollContainer}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {images.map((image, index) => {
                return renderImage(image, index);
              })}
            </ScrollView>
          </View>
          <View style={styles.buttonContainer}>
            <View style={styles.button}>
              <Button
                title="Cancel"
                onPress={handleCancel}
                color={colors.primary}
              />
            </View>
            <View style={styles.button}>
              <Button
                title="Add"
                onPress={handleAddProduct}
                disabled={isUploading}
                color={colors.primary}
              />
            </View>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AddProductScreen;
