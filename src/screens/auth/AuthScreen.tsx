/* eslint-disable curly */
/* eslint-disable react-native/no-inline-styles */
import {useTheme} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  View,
  Button,
  TouchableOpacity,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Image,
} from 'react-native';
import TextInputApp from '../../components/text-input/TextInputAuth';
import {AppColors} from '../../config/app-colors';
import {ButtonsOpacity} from '../../config/buttons-opacity';
import {
  validateEmail,
  validateNumber,
  validatePassword,
  validateUsername,
} from '../../helpers/strings/auth';
import {useAppDispatch, useAppSelector} from '../../hooks/redux';
import Preloader from '../preloader/Preloader';
import * as appActionCreator from '../../store/actions';
import {styles} from './styles';
import {loadImages} from '../../helpers/images/image-from-library';
import {Asset} from 'react-native-image-picker';

const AuthScreen = () => {
  const {colors} = useTheme();
  const [isRegister, setIsRegister] = useState(false);
  const [isUploading, setIsUploading] = useState(false);

  const [email, setEmail] = useState('');
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [profileImage, setProfileImage] = useState<Asset | undefined>(
    undefined,
  );

  const {preloaderAuth, errorMessage} = useAppSelector(state => ({
    preloaderAuth: state.preloaderAuth,
    errorMessage: state.errorMessage,
  }));
  const dispatch = useAppDispatch();

  const handleChangeAuthMethod = () => {
    setEmail('');
    setName('');
    setPassword('');
    setPhoneNumber('');
    setProfileImage(undefined);
    setIsRegister(state => !state);
  };

  const handleKeyboardDismiss = () => {
    Keyboard.dismiss();
  };

  const handleImageUpload = async () => {
    setIsUploading(true);
    const file = await loadImages();
    if (!file) {
      setIsUploading(false);
      return;
    }
    setProfileImage(file);
    setIsUploading(false);
  };

  const handleUserAuth = () => {
    const isValidEmail = validateEmail(email);
    const isValidPassword = validatePassword(password);
    if (!isValidEmail || !isValidPassword) {
      Alert.alert('Invalid credentials', 'Enter valid data');
      return;
    }
    if (!isRegister) dispatch(appActionCreator.login({email, password}));
    else {
      const isValidName = validateUsername(name);
      const isValidNumber = validateNumber(phoneNumber);
      if (!isValidName || !isValidNumber) {
        Alert.alert('Invalid credentials', 'Enter valid data');
        return;
      }
      dispatch(
        appActionCreator.register({
          email,
          password,
          name,
          phoneNumber,
          avatar: profileImage,
        }),
      );
    }
  };

  if (preloaderAuth) {
    return <Preloader />;
  }

  return (
    <TouchableWithoutFeedback onPress={handleKeyboardDismiss}>
      <View style={styles.screen}>
        <View
          style={{
            ...styles.formContainer,
            backgroundColor: colors.card,
            height: isRegister ? '80%' : '60%',
          }}>
          <TextInputApp
            value={email}
            keyboardType="email-address"
            onChange={setEmail}
            placeholder="Enter email"
          />
          {isRegister && (
            <>
              <TextInputApp
                value={name}
                onChange={setName}
                placeholder="Enter name"
              />
              <TextInputApp
                value={phoneNumber}
                onChange={setPhoneNumber}
                keyboardType="phone-pad"
                placeholder="Enter phone number"
              />
            </>
          )}
          <TextInputApp
            value={password}
            onChange={setPassword}
            isPassword={true}
            placeholder="Enter password"
          />
          {isRegister && (
            <View style={styles.imageContainer}>
              <Button
                onPress={handleImageUpload}
                color={colors.primary}
                disabled={isUploading}
                title={'Add avatar'}
              />
              <Image source={{width: 55, height: 55, uri: profileImage?.uri}} />
            </View>
          )}
          {errorMessage && (
            <Text style={{...styles.testLink, color: AppColors.ERROR_TEXT}}>
              {errorMessage}
            </Text>
          )}
          <View style={styles.button}>
            <Button
              onPress={handleUserAuth}
              color={colors.primary}
              disabled={isUploading}
              title={isRegister ? 'Register' : 'Login'}
            />
          </View>
          <TouchableOpacity
            activeOpacity={ButtonsOpacity.LINKS}
            onPress={handleChangeAuthMethod}>
            <Text style={{...styles.testLink, color: colors.primary}}>
              {isRegister ? 'Back to Login' : 'Register'}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default AuthScreen;
