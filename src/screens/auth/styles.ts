import {StyleSheet} from 'react-native';
import {TextSize} from '../../common/enums/components/text-size';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    width: '70%',
    borderRadius: 50,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  button: {
    width: 100,
    height: 50,
  },
  testLink: {
    fontSize: TextSize.MAIN,
  },
  imageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export {styles};
