import React, {useEffect} from 'react';
import {View, FlatList, Button} from 'react-native';
import FloatingButton from '../../components/market-list/floating-button/FloatingButton';
import ProductPreview from '../../components/market-list/product-preview/ProductPreview';
import SearchBar from '../../components/market-list/search-bar/SearchBar';
import {AppColors} from '../../config/app-colors';
import {useAppDispatch, useAppSelector} from '../../hooks/redux';
import * as appActionCreator from '../../store/actions';
import Preloader from '../preloader/Preloader';
import {styles} from './styles';

const loadProductsFilter = {
  page: 1,
  perPage: 10,
  filter: '',
};

const MarketListScreen = () => {
  const {products, currentUser, preloader} = useAppSelector(state => ({
    products: state.products,
    currentUser: state.currentUser,
    preloader: state.preloader,
  }));
  const dispatch = useAppDispatch();

  const loadMoreProducts = () => {
    loadProductsFilter.page += 1;
    if (currentUser) {
      dispatch(
        appActionCreator.loadProducts({
          token: currentUser.token,
          filters: loadProductsFilter,
          isLoadMore: true,
        }),
      );
    }
  };

  const filterProducts = (filterText: string) => {
    loadProductsFilter.page = 1;
    loadProductsFilter.filter = filterText;
    if (currentUser) {
      dispatch(
        appActionCreator.loadProducts({
          token: currentUser.token,
          filters: loadProductsFilter,
        }),
      );
    }
  };

  const handleLogout = () => {
    dispatch(appActionCreator.logout());
  };

  useEffect(() => {
    if (currentUser) {
      dispatch(
        appActionCreator.loadProducts({
          token: currentUser.token,
          filters: loadProductsFilter,
        }),
      );
    }
  }, [dispatch, currentUser]);

  if (preloader) {
    return <Preloader />;
  }

  return (
    <View style={styles.screen}>
      <View style={styles.buttonLogout}>
        <Button
          title="Logout"
          color={AppColors.ERROR_TEXT}
          onPress={handleLogout}
        />
      </View>

      <SearchBar onSearch={filterProducts} />
      <View style={styles.productsList}>
        <FlatList
          onEndReachedThreshold={0.1}
          onEndReached={loadMoreProducts}
          data={products}
          refreshing={preloader}
          onRefresh={() => filterProducts('')}
          showsVerticalScrollIndicator={false}
          renderItem={item => {
            return <ProductPreview product={item.item} />;
          }}
        />
      </View>
      <FloatingButton />
    </View>
  );
};

export default MarketListScreen;
