import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  productsList: {
    width: '100%',
    flex: 1,
    alignItems: 'center',
  },
  buttonLogout: {
    width: '30%',
    alignSelf: 'center',
    marginTop: 10,
  },
});

export {styles};
