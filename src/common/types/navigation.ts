import {NativeStackNavigationProp} from '@react-navigation/native-stack';

export type RootStackParamList = {
  MessageList: undefined;
  ProductDetails: {
    productId: number;
  };
  AddProduct: undefined;
  Auth: undefined;
};

export type MessageListScreenProp = NativeStackNavigationProp<
  RootStackParamList,
  'MessageList'
>;

export type ProductDetailsScreenProp = NativeStackNavigationProp<
  RootStackParamList,
  'ProductDetails'
>;

export type AddProductScreenProp = NativeStackNavigationProp<
  RootStackParamList,
  'AddProduct'
>;

export type ProductDetailsScreenRouteProp = {
  params: {
    productId: number;
  };
};
