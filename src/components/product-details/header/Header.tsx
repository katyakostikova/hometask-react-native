import {useNavigation, useTheme} from '@react-navigation/native';
import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {IconNames} from '../../../common/enums/components/icon-names';
import {IconSizes} from '../../../common/enums/components/icon-sizes';
import {ButtonsOpacity} from '../../../config/buttons-opacity';
import {styles} from './styles';

const Header = () => {
  const {colors} = useTheme();
  const navigation = useNavigation();

  const handleBackButton = () => {
    navigation.goBack();
  };
  return (
    <View style={{...styles.header, backgroundColor: colors.card}}>
      <TouchableOpacity
        activeOpacity={ButtonsOpacity.ICONS}
        onPress={handleBackButton}>
        <Icon
          name={IconNames.BACK}
          size={IconSizes.DEFAULT_ICON}
          color={colors.primary}
        />
      </TouchableOpacity>

      <Text style={{...styles.text, color: colors.primary}}>Back</Text>
    </View>
  );
};

export default Header;
