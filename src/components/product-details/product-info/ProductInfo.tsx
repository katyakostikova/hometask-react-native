import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './styles';

interface ProductInfoProps {
  title?: string;
  price?: number;
  description?: string;
}

const ProductInfo = ({title, description, price}: ProductInfoProps) => {
  const {colors} = useTheme();
  return (
    <View style={{...styles.content, backgroundColor: colors.card}}>
      <View style={styles.header}>
        <Text style={{...styles.headerText, color: colors.text}}>{title}</Text>
        <Text style={{...styles.headerText, color: colors.text}}>${price}</Text>
      </View>
      <View>
        <Text style={{...styles.description, color: colors.text}}>
          {description}
        </Text>
      </View>
    </View>
  );
};

export default ProductInfo;
