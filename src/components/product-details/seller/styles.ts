import {StyleSheet} from 'react-native';
import {TextSize} from '../../../common/enums/components/text-size';

const styles = StyleSheet.create({
  content: {
    width: '100%',
  },
  sellerInfoContainer: {
    flexDirection: 'row',
    width: '50%',
    height: '55%',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  imageContainer: {
    width: '30%',
    height: '65%',
    borderRadius: 40,
    overflow: 'hidden',
  },
  avatar: {
    width: '100%',
    height: '100%',
  },
  sellerInfo: {
    height: '45%',
  },
  sellerName: {
    fontSize: TextSize.TITLE,
    padding: 5,
    paddingLeft: 15,
    fontWeight: 'bold',
  },
  sellerNumber: {
    paddingLeft: 10,
  },
  buttonContainer: {
    width: '50%',
    height: '30%',
    marginLeft: 20,
    alignItems: 'flex-start',
  },
});

export {styles};
