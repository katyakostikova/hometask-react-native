import {StyleSheet} from 'react-native';
import {TextSize} from '../../../../common/enums/components/text-size';
import {AppColors} from '../../../../config/app-colors';

const styles = StyleSheet.create({
  button: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconContainer: {
    transform: [{rotateY: '180deg'}],
  },
  text: {
    fontSize: TextSize.MAIN,
    color: AppColors.CALL_BUTTON,
    marginRight: 4,
  },
});

export {styles};
