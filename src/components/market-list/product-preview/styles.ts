import {StyleSheet} from 'react-native';
import {TextSize} from '../../../common/enums/components/text-size';

const styles = StyleSheet.create({
  card: {
    marginHorizontal: 10,
    marginVertical: 10,
    paddingVertical: 20,
    paddingHorizontal: 10,
    height: 140,
    elevation: 10,
    shadowOpacity: 0.36,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowColor: 'black',
    backgroundColor: 'white',
    borderRadius: 20,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageContainer: {
    marginRight: 5,
    width: '30%',
  },
  image: {
    width: 100,
    height: 100,
  },
  productInfo: {
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    width: '70%',
  },
  productHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    height: '25%',
  },
  textDescriptionContainer: {
    height: '75%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontSize: TextSize.TITLE,
  },
});

export {styles};
