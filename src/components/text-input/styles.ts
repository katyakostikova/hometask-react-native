import {StyleSheet} from 'react-native';
import {TextSize} from '../../common/enums/components/text-size';

const styles = StyleSheet.create({
  inputContainer: {
    width: '80%',
    height: '12%',
    margin: 10,
  },
  border: {
    borderWidth: 1,
  },
  input: {
    fontSize: TextSize.MAIN,
  },
});

export {styles};
