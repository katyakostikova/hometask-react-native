import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import React from 'react';
import {useColorScheme} from 'react-native';
import {RootStackParamList} from '../common/types/navigation';
import {
  NavigationThemeDark,
  NavigationThemeLight,
} from '../config/navigation-theme';
import {useAppSelector} from '../hooks/redux';
import AddProductScreen from '../screens/add-product/AddProductScreen';
import AuthScreen from '../screens/auth/AuthScreen';
import MarketListScreen from '../screens/market-list/MarketListScreen';
import ProductDetailsScreen from '../screens/product-details/ProductDetailsScreen';

const Stack = createNativeStackNavigator<RootStackParamList>();

const screenOptions = {
  headerShown: false,
};

const RootNavigator: React.FC = () => {
  const currentUser = useAppSelector(state => state.currentUser);
  const colorScheme = useColorScheme();

  return (
    <NavigationContainer
      theme={
        colorScheme === 'dark' ? NavigationThemeDark : NavigationThemeLight
      }>
      <Stack.Navigator
        initialRouteName="MessageList"
        screenOptions={screenOptions}>
        {currentUser ? (
          <>
            <Stack.Screen name="MessageList" component={MarketListScreen} />
            <Stack.Screen
              name="ProductDetails"
              component={ProductDetailsScreen}
            />
            <Stack.Screen name="AddProduct" component={AddProductScreen} />
          </>
        ) : (
          <Stack.Screen name="Auth" component={AuthScreen} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
