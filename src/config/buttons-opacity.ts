const ButtonsOpacity = {
  ICONS: 0.75,
  PRODUCT_CARD: 0.8,
  LINKS: 0.7,
  FLOATING_BUTTON: 0.8,
};
export {ButtonsOpacity};
