import {AppColors} from './app-colors';

const NavigationThemeLight = {
  dark: false,
  colors: {
    primary: AppColors.PRIMARY_LIGHT,
    background: AppColors.BACKGROUND_LIGHT,
    card: AppColors.CARD_LIGHT,
    text: AppColors.TEXT_LIGHT,
    border: AppColors.PLACEHOLDER_LIGHT,
    notification: 'rgb(255, 69, 58)',
  },
};

const NavigationThemeDark = {
  dark: false,
  colors: {
    primary: AppColors.PRIMARY_DARK,
    background: AppColors.BACKGROUND_DARK,
    card: AppColors.CARD_DARK,
    text: AppColors.TEXT_DARK,
    border: AppColors.PLACEHOLDER_DARK,
    notification: 'rgb(255, 69, 58)',
  },
};

export {NavigationThemeLight, NavigationThemeDark};
