import {launchImageLibrary} from 'react-native-image-picker';

const loadImages = async () => {
  const images = await launchImageLibrary({
    mediaType: 'photo',
    includeBase64: true,
  });

  return images.assets ? images.assets[0] : null;
};

export {loadImages};
